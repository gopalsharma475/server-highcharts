var Order = require('../models/order');
var moment = require('moment');

/**
 * @name orderList
 * @param {*} req 
 * @param {*} res
 * @description This apis created for general testing purpose. This Api will return top 20 product from product and order collections
 * @createdBy Gopal Sharma
 */
exports.orderList = function (req, res) {

    console.log(">>>>>>>>>",moment("2021-01-10").startOf('day').toDate());
    console.log(">>>>>>>>>",moment("2021-01-23").endOf('day').toDate());
    Order.aggregate([
        {
            $match: {
                "order_placed_at": {
                    $gte: moment("2021-01-10").startOf('day').toDate(),
                    $lte: moment("2021-01-23").endOf('day').toDate(),
                }
            }
        },
        {
            $group: {
                _id: '$product_id',
                orderCount: {
                    $sum: 1
                }
            }
        },
        {
            $sort: {
                orderCount: -1
            }
        },
        {
            $limit: 20
        }
    ], function (err, records) {
        if(err) {
            res.send({
               message : 'Something went wrong',
               data : [] 
            })
        }
        res.send({
            message : 'success',
            data : records
        })
    })
}

/**
 * @name orderSales
 * @param {*} req 
 * @param {*} res
 * @description This apis created for general testing purpose. This Api will return total sales of last week and present week.
 * @createdBy Gopal Sharma
 */
exports.orderSales = function (req, res) {
    console.log(">>>>>>>>>",moment("2021-01-10").startOf('day').toDate());
    console.log(">>>>>>>>>",moment("2021-01-23").endOf('day').toDate());
    Order.aggregate([
        {
            $match: {
                //ISODate("2021-01-10T00:00:00.000Z")
                //moment("2021-01-23").endOf('day').toDate()
                "order_placed_at": {
                    $gte: moment("2021-01-10").startOf('day').toDate(),
                    $lte: moment("2021-01-16").endOf('day').toDate(),
                }
            }
        },
        {
            "$lookup": {
                "from": "products",
                "let": { "product_id": "$product_id" },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": { "$eq": ["$_id", "$$product_id"] }
                        }
                    },
                    {
                        $project: {
                            product_price: 1
                        }
                    }
                ],
                "as": "product_id"
            }
        },
        {
            $project: {
                product_id: {
                    $arrayElemAt: ['$product_id', 0]
                },
                order_placed_at: 1,
                order_placed_date: {
                    $dateToString: {
                        format: '%Y-%m-%d', date: '$order_placed_at'
                    }
                }
            }
        },
        {
            $group: {
                _id: '$order_placed_date',
                product_price: {
                    $sum: '$product_id.product_price'
                }
            }
        },
        {
            $sort: {
                _id: 1
            }
        },
        {
            $group: {
                _id: null,
                tototalSale: {
                    $sum: '$product_price'
                },
                days: {
                    $push: '$$ROOT'
                }
            }
        }
    ], function (err, lastWeekData) {
        if(err) {
            res.send({
               message : 'Something went wrong',
               data : [] 
            })
        }
        // Second Query Start
        Order.aggregate([
            {
                $match: {
                    "order_placed_at": {
                        // $gte: ISODate("2021-01-17T00:00:00.000Z"),
                        // $lte: ISODate("2021-01-23T23:59:59.000Z"),
                        $gte: moment("2021-01-17").startOf('day').toDate(),
                        $lte: moment("2021-01-23").endOf('day').toDate(),
                    }
                }
            },
            {
                "$lookup": {
                    "from": "products",
                    "let": { "product_id": "$product_id" },
                    "pipeline": [
                        {
                            "$match": {
                                "$expr": { "$eq": ["$_id", "$$product_id"] }
                            }
                        },
                        {
                            $project: {
                                product_price: 1
                            }
                        }
                    ],
                    "as": "product_id"
                }
            },
            {
                $project: {
                    product_id: {
                        $arrayElemAt: ['$product_id', 0]
                    },
                    order_placed_at: 1,
                    order_placed_date: {
                        $dateToString: {
                            format: '%Y-%m-%d', date: '$order_placed_at'
                        }
                    }
                }
            },
            {
                $group: {
                    _id: '$order_placed_date',
                    product_price: {
                        $sum: '$product_id.product_price'
                    }
                }
            },
            {
                $sort: {
                    _id: 1
                }
            },
            {
                $group: {
                    _id: null,
                    tototalSale: {
                        $sum: '$product_price'
                    },
                    days: {
                        $push: '$$ROOT'
                    }
                }
            }
        ],function(err,currentWeekData){
            if(err) {
                res.send({
                   message : 'Something went wrong',
                   data : [] 
                })
            }
            res.send({
                message : 'success',
                lastWeekData : lastWeekData,
                currentWeekData : currentWeekData
            })
        })

        //  Second Query End
        
    })
}

/**
 * @name orderSales
 * @param {*} req 
 * @param {*} res
 * @description This apis created for general testing purpose. This Api will return total orders of last week and present week.
 * @createdBy Gopal Sharma
*/
exports.orderPerWeek = function (req, res) {
    console.log(">>>>>>>>>",moment("2021-01-10").startOf('day').toDate());
    console.log(">>>>>>>>>",moment("2021-01-23").endOf('day').toDate());
    Order.aggregate([
        {
            $match: {
                "order_placed_at": {
                    // $gte: ISODate("2021-01-10T00:00:00.000Z"),
                    // $lte: ISODate("2021-01-16T23:59:59.000Z"),
                    $gte: moment("2021-01-10").startOf('day').toDate(),
                    $lte: moment("2021-01-16").endOf('day').toDate(),
                }
            }
        },
        {
            $project: {
                product_id: 1,
                order_placed_at: 1,
                order_placed_date: {
                    $dateToString: {
                        format: '%Y-%m-%d', date: '$order_placed_at'
                    }
                }
            }
        },
        {
            $group: {
                _id: '$order_placed_date',
                orderCount: {
                    $sum: 1
                }
            }
        },
        {
            $sort: {
                _id: 1
            }
        },
        {
            $group: {
                _id: null,
                tototalOrders: {
                    $sum: '$orderCount'
                },
                days: {
                    $push: '$$ROOT'
                }
            }
        }
    ], function (err, lastWeekData) {
        if(err) {
            res.send({
               message : 'Something went wrong',
               data : [] 
            })
        }
        // Second Query Start
        Order.aggregate([
            {
                $match: {
                    "order_placed_at": {
                        // $gte: ISODate("2021-01-17T00:00:00.000Z"),
                        // $lte: ISODate("2021-01-23T23:59:59.000Z"),
                        $gte: moment("2021-01-17").startOf('day').toDate(),
                        $lte: moment("2021-01-23").endOf('day').toDate(),
                    }
                }
            },
            {
                $project: {
                    product_id: 1,
                    order_placed_at: 1,
                    order_placed_date: {
                        $dateToString: {
                            format: '%Y-%m-%d', date: '$order_placed_at'
                        }
                    }
                }
            },
            {
                $group: {
                    _id: '$order_placed_date',
                    orderCount: {
                        $sum: 1
                    }
                }
            },
            {
                $sort: {
                    _id: 1
                }
            },
            {
                $group: {
                    _id: null,
                    tototalOrders: {
                        $sum: '$orderCount'
                    },
                    days: {
                        $push: '$$ROOT'
                    }
                }
            }
        ],function(err,currentWeekData){
            if(err) {
                res.send({
                   message : 'Something went wrong',
                   data : [] 
                })
            }
            res.send({
                message : 'success',
                lastWeekData : lastWeekData,
                currentWeekData : currentWeekData
            })
        })
        //  Second Query End
        
    })
}













