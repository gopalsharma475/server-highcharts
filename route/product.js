var Product = require('../models/product');

/**
 * @name productList
 * @param {*} req 
 * @param {*} res
 * @description This apis created for general testing purpose. This Api will return product list
 * @createdBy Gopal Sharma
 */
exports.productList = function (req, res) {
    Product.find((err , records) => {
        if(err) {
            res.send({
               message : 'Something went wrong',
               data : [] 
            })
        }
        res.send({
            message : 'success',
            data : records
        })
    })
}













