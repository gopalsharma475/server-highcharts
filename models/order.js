var mongoose = require('mongoose'); 

var orderSchema = mongoose.Schema({  
    product_id:  { type: String },
    order_placed_at  :   { type: Date, default: Date.now }, 
    created_by :   { type: Date, default: Date.now },
    modified_by :   { type: Date, default: Date.now },
	created_at  :   { type: Date, default: Date.now },  
    modified_at :   { type: Date, default: Date.now }
}); 

module.exports = mongoose.model('Order', orderSchema); 