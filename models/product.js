var mongoose = require('mongoose'); 

var productSchema = mongoose.Schema({  
    product_name:  { type: String },
    product_price :   { type: Number },
    product_sku_number :   { type: String },
    created_by :   { type: Number },
    modified_by :   { type: Number },
	created_at  :   { type: Date, default: Date.now },  
    modified_at :   { type: Date, default: Date.now }
}); 

module.exports = mongoose.model('Product', productSchema); 